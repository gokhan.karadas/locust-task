from locust import HttpLocust, TaskSet, task



class UserBehavior(TaskSet):
    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """




    @task(1)
    def index(self):
        self.client.get("wp-json/posts")



class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 6000
    max_wait = 15000
